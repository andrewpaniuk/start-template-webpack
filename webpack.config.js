const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const SASSExtract = require('./webpack/extractSass');
const devServer = require('./webpack/devServer');
const Sass = require('./webpack/sass');
const Css = require('./webpack/css');
const babel = require('./webpack/babel');

process.traceDeprecation = true;

const common = merge([{
    devtool: 'source-map',
    entry: './src/js/app.js',

    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/bundle.js'
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.pug'
      })
    ],
  },
  pug(),
  babel()
]);


module.exports = function (env) {
  if (env === 'prod') {
    return merge([
      common,
      SASSExtract(),
    ]);
  }

  if (env === 'dev') {
    return merge([
      common,
      devServer(),
      Sass(),
      Css()
    ]);
  }
};